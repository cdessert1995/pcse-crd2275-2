#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<omp.h>
#include<time.h>
#include"mpi.h"
#define _NUM_THREADS 16

int set = 60000;

void expand_int(int* small, int row, int col, double *large);
void expand_double(double* small, int row, int col, double* large);
void sigmoid(double* input, int row, int col);
double cost_function(double hidden_weights[25][785], double outer_weights[10][26], double train[set][784],int label[set],int y[set][10]);
void delta(double hidden_error[set][10], double outer_error[set][26], double hidden_weights[25][785], double outer_weights[10][26],int y[set][10],double train[set][784]);
double sigmoid_num(double num);
int gradient_descent(double hidden_weights[25][785], double outer_weights[10][26], double train[set][784],int label[set]);

int main(int argc, char **argv){

  omp_set_num_threads(_NUM_THREADS);
  int required;
  int provided;
  MPI_Init_thread(&argc,&argv,required,provided);
  int rank;
  int size;
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  if(rank == 0){
  	printf("Loading training data\n");
  }
  
	//Read in the file
  FILE *fp;
  fp = fopen("mnist_train.csv","r");

  double train[set][784];
  int label[set];

  int i,j;
  int temp;

	for(i=0;i<set;i++){
    fscanf(fp,"%d%*c",&temp);
    label[i] = temp;
    for(j=0;j<784;j++){
      fscanf(fp,"%d%*c",&temp);
      train[i][j] = ((double) temp)/255.0;
    }
  } 
  fclose(fp); 

  //Create hidden and outer weights  
  double hidden_weights[25][785];
  double outer_weights[10][26];
  //and then initialize them randomly
  srand(time(NULL)); 

  #pragma omp parallel
  {
  #pragma omp for schedule(static) 
  for(i=0;i<25;i++){
    for(j=0;j<785;j++){ 
      hidden_weights[i][j] = 86.0*((double)rand()/RAND_MAX-0.5)/500.0;
    } 
  }
  #pragma omp for schedule(static)
  for(i=0;i<10;i++){
    for(j=0;j<26;j++){
      outer_weights[i][j] = 414.0*((double)rand()/RAND_MAX-0.5)/500.0;
	}
  } 
  }
	
  //Perform gradient descent to optimize the weights
  double tstart = clock();
  int iterations = gradient_descent(hidden_weights,outer_weights,train,label);		
  double tend = clock();

  if(rank == 0){
  	printf("Time per iteration: %f\n", (tend-tstart)/((double)(iterations*CLOCKS_PER_SEC))); 
  	printf("Finding accuracy...\n"); 
  }
	
  //Load in test data
  FILE *fq;
  fq = fopen("mnist_test.csv","r");

  double test[10000][784];
  int test_label[10000];
 
  for(i=0;i<10000;i++){
    fscanf(fq,"%d%*c",&temp); 
    test_label[i] = temp;
    for(j=0;j<784;j++){
      fscanf(fq,"%d%*c",&temp);
      test[i][j] = ((double) temp)/255.0;
    }   
  }    
  fclose(fq);

  //Compute predictions on test data
  char *lN = "N";
  char *lT = "T";

  int ten = 10; 
  int tfive = 25; 
  int tsix = 26; 
  int seveif = 785;
  int tenth = 10000;
  double gamma = 1.0;
  double beta = 0.0;

  double a_1b[10000][785];
  double z_2[10000][25];
  double a_2b[10000][26];
  double z_3[10000][10];

  //Calculate all matrices in forward propagation
  expand_double(test,tenth,784,a_1b);
  dgemm(lN,lT,&tenth,&tfive,&seveif,&gamma,a_1b,&tenth,hidden_weights,&tfive,&beta,z_2,&tenth);
  sigmoid(z_2,tenth,25);
  expand_double(z_2,tenth,25,a_2b);
  dgemm(lN,lT,&tenth,&ten,&tsix,&gamma,a_2b,&tenth,outer_weights,&ten,&beta,z_3,&tenth);
  sigmoid(z_3,tenth,10);

  //Determine accuracy of predictions on test data
  int max_index = 0;
  double max_num = 0.0;
  int num_correct = 0;
  #pragma omp parallel 
  {
  #pragma omp for schedule(static) reduction(+:num_correct) firstprivate(max_index,max_num)  
  for(i=0;i<10000;i++){
  max_index = 0;
  max_num = 0.0;
    for(j=0;j<10;j++){
      if(z_3[i][j] > max_num){
        max_num = z_3[i][j];
        max_index = j;
      }
    }
    if(test_label[i] == max_index){
      num_correct += 1;
    }
  }
  }

  int accuracy;
  MPI_Reduce(&num_correct,&accuracy,1,MPI_INT,MPI_MAX,0,MPI_COMM_WORLD);

  if(rank == 0){
	printf("Accuracy = %6.3f\%\n",((double) accuracy)/100.0);
  }
	
  MPI_Finalize();
	
  return 0;
}

int gradient_descent(double hidden_weights[25][785], double outer_weights[10][26], double train[set][784],int label[set]){
  int iterations = 500;
  int i,j,m = 0;
  double cost = 0.0;
  int y[set][10];
  double hidden_error[set][26];
  double outer_error[set][10];
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	
  //Initialize all matrices to zero using Chris' weird method
  #pragma omp parallel 
  {
  #pragma omp for schedule(static)
  for(i=0;i<set;i++){
    for(j=0;j<10;j++){
      outer_error[i][j] = 0.0;
      hidden_error[i][j] = 0.0;
    }
    for(j=10;j<26;j++){
      hidden_error[i][j] = 0.0;
    }
  }

  //Create matrix of correct outputs
  #pragma omp for schedule(static)
  for(i=0;i<set;i++){
    for(j=0;j<10;j++){
      y[i][j] = 0;
    }
  }
	
  #pragma omp for schedule(dynamic)
  for(i=0;i<set;i++){
    y[i][label[i]] = 1;
  }
  }

  if(rank == 0){
  	printf("Starting gradient descent\n");
  }
	
  //Perform gradient descent on the weights
  for(m=0;m<iterations;m++){
  //Calculate the current cost and print it out
  	cost = cost_function(hidden_weights,outer_weights,train,label,y);		
	if(rank == 0){
      printf("Iteration:%5d, COST: %f\n",m,cost);
    }
	//Find the gradient
	delta(hidden_error,outer_error,hidden_weights,outer_weights,y,train);
  }	
  if(rank == 0){
    printf("Finished gradient descent\n");
  }
	
  return m;
}

void delta(double hidden_error[set][26], double outer_error[set][10], double hidden_weights[25][785], double outer_weights[10][26],int y[set][10],double train[set][784]){

  int i,j,k,l;

  char *lN = "N";
  char *lT = "T";

  int ten = 10;
  int tfive = 25;
  int tsix = 26;
  int seveif = 785;
  double gamma = 1.0;
  double beta = 0.0;

  double a_1b[set][785];
  double z_2[set][25];
  double a_2b[set][26];
  double z_3[set][10];

  //Calculate all matrices in forward propagation
  expand_double(train,set,784,a_1b);
  dgemm(lN,lT,&set,&tfive,&seveif,&gamma,a_1b,&set,hidden_weights,&tfive,&beta,z_2,&set);
  sigmoid(z_2,set,25);
  expand_double(z_2,set,25,a_2b);
  dgemm(lN,lT,&set,&ten,&tsix,&gamma,a_2b,&set,outer_weights,&ten,&beta,z_3,&set);
  sigmoid(z_3,set,10);

  //Find the error of the outer weights
  #pragma omp parallel 
  {
  #pragma omp for schedule(static) 
  for(i=0;i<set;i++){
    for(j=0;j<10;j++){
      outer_error[i][j] = z_3[i][j] - y[i][j];
    }
  }
  }
  
  //Calculate the unregularized backpropagation delta
  double result[set][26];
  dgemm(lN,lN,&set,&tsix,&ten,&gamma,outer_error,&set,outer_weights,&ten,&beta,result,&set);  

  double sigmoidGradientz_2b[set][26];
  double z_2b[set][25];

  expand_double(z_2,set,25,z_2b);

  #pragma omp parallel 
  {
  #pragma omp for schedule(static) 
  for(i=0;i<set;i++){
    for(j=0;j<26;j++){
      sigmoidGradientz_2b[i][j] = sigmoid_num(z_2b[i][j])*(1.0-sigmoid_num(z_2b[i][j]));
    }
  }
	
  #pragma omp for schedule(static)
  for(i=0;i<set;i++){
    for(j=0;j<26;j++){
      hidden_error[i][j] = result[i][j]*sigmoidGradientz_2b[i][j];
    }
  }
  }

  double delta_1[785][26];
  dgemm(lT,lN,&seveif,&tsix,&set,&gamma,a_1b,&set,hidden_error,&set,&beta,delta_1,&seveif);
  double delta_2[26][10];
  dgemm(lT,lN,&tsix,&ten,&set,&gamma,a_2b,&set,outer_error,&set,&beta,delta_2,&tsix);

  double unreg_grad_1[785][26];
  double unreg_grad_2[26][10];
  double lambda = 1.0;
  double alpha = 0.00001;


  #pragma omp parallel 
  {
  #pragma omp for schedule(static) 
  for(i=0;i<785;i++){
    for(j=0;j<26;j++){
      unreg_grad_1[i][j] = delta_1[i][j]/set;
    }
  }
  #pragma omp for schedule(static)
  for(i=0;i<26;i++){
    for(j=0;j<10;j++){
      unreg_grad_2[i][j] = delta_2[i][j]/set;
    }
  }
  }	
  
  //Take away the gradient from the weights
  #pragma omp critical
  { 
  for(i=0;i<25;i++){
    for(j=0;j<785;j++){
      hidden_weights[i][j] -= alpha*unreg_grad_1[i][j];
    }
  }
  }
  #pragma omp critical
  {
  for(i=0;i<10;i++){
    for(j=0;j<26;j++){
      outer_weights[i][j] -= alpha*unreg_grad_2[i][j];
    }
  }
  }
}

double cost_function(double hidden_weights[25][785], double outer_weights[10][26], double train[set][784],int label[set],int y[set][10]){
  double J = 0.0;
  int i,j;

  double a_1b[set][785];
  double z_2[set][25];
  double a_2b[set][26];
  double z_3[set][10];

  char *lN = "N";
  char *lT = "T";
	
  int ten = 10;
  int tfive = 25;
  int tsix = 26;
  int seveif = 785;
  double gamma = 1.0;
  double beta = 0.0;

  //Calculate the matrices for forward propagation
  expand_double(train,set,784,a_1b);
  dgemm(lN,lT,&set,&tfive,&seveif,&gamma,a_1b,&set,hidden_weights,&tfive,&beta,z_2,&set);
  sigmoid(z_2,set,25);
  expand_double(z_2,set,25,a_2b);
  dgemm(lN,lT,&set,&ten,&tsix,&gamma,a_2b,&set,outer_weights,&ten,&beta,z_3,&set);
  sigmoid(z_3,set,10);

  #pragma omp parallel 
  {
  #pragma omp for schedule(static) reduction(+:J) 
  for(i=0;i<set;i++){
    for(j=0;j<10;j++){
    	J += -1.0*y[i][j]*log(z_3[i][j])-(1.0-y[i][j])*(log(1.0-z_3[i][j])); 
    }
  }
  }
  J = J/((double) set);
  
  return J;
}

//This function takes a matrix and returns the same matrix but with an extra column to the right
//The extra columns is all 1's
//This is used for introducting the bias units of a layer
void expand_double(double* small, int row, int col,double* large){
  int i,j;
  int k=0;
  
  #pragma omp parallel 
  {
  #pragma omp for schedule(dynamic)
  for(i=0;i<row;i++){
    for(j=0;j<col;j++){
      large[i*(col)+j+k] = small[i*col+j];
      if((i*col+j)%col == col-1){
        k+=1;
      }
    }
  }
  #pragma omp for schedule(dynamic)
  for(i=col;i<row*(col+1)+1;i+=col+1){
    large[i] = 1.0;
  }
  }
}

//This function performs an element-wise sigmoid on a matrix
void sigmoid(double* input, int row, int col){
  int i,j;
  #pragma omp parallel 
  {
  #pragma omp for schedule(static)
  for(i=0;i<row;i++){
	for(j=0;j<col;j++){
      input[i*col+j] = 1/(1+exp(-1*input[i*col+j]));
	}
  }  
  }
}

//This function performs the sigmoid function on a single number
double sigmoid_num(double num){
  return 1/(1+exp(-1*num));
}
